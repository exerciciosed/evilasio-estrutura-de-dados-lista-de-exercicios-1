package ListaExercicios1.Questao3


object MainQuestao3 {
     def main(args: Array[String]) {
      var l:ListaDEOrdenada = null
       
      l = new ListaDEOrdenada()
      println("Lista Vazia Criada") 
       
      l.add(1)
      l.add(2)
      l.add(3)
      l.add(4)
      l.add(5)
      l.add(6)
      l.add(200)
      println("Valores 1 a 6 e 200 adicionados sucessivamente na lista") 
      
      print("Lista impressa iterativamente: ")
      l.printList() 
      
      print("Lista impressa de tras pra frente: ")
      l.printListReverse()
      println()
      
      if(l.isEmpty() == 1)
        println("Lista vazia") 
      else 
        println("Lista nao vazia")
      
      println("O valor 6 est� na posicao "+ (l.search(6)+1))    

      l.remove(l.search(5))
      println("Removido o elemento de valor 5 iterativamente")
      
      l.removeRec(l.search(200))
      println("Removido o elemento de valor 200 recursivamente")

      print("Lista apos remocoes impressa recursivamente: ")
      l.printListRecursion()
      println()
      
      var l2 = new ListaDEOrdenada() 
      l2.add(1); l2.add(2); l2.add(3); l2.add(4); l2.add(6)
      print("Verifica se lista atual e igual a lista: ")
      l2.printListRecursion()
      println()
      if(l.equalsList(l2)){
        println("Listas iguais")
      }
      else
        println("Listas diferentes")
      
      
      l.freeList()
      println("Lista liberada")
   }
}