package ListaExercicios1.Questao3
import ListaExercicios1._

//---------- Lista Duplamente Encadeada Ordenada ----------


class ListaDEOrdenada(var topo:CelulaDouble[Int]=null){
    
    /*
     *insere um elemento na lista 
     */
    def add(valor:Int){
        var c = new CelulaDouble[Int](valor, null ,null)
        if(topo == null)
            addFirstCel(c)
        else{
            var ant:CelulaDouble[Int] = null
            var aux = topo
            while(aux!=null && aux.valor <= c.valor){
              ant = aux
              aux = aux.proximo 
            }
            if(ant==null){ //Quando deve inserir na primeira posicao
              c.proximo = aux
              topo = c
            }else{
              ant.proximo = c
              c.anterior = ant
              if(aux!=null)//Caso nao seja o ultimo valor, faz o proximo de c apontar para aux
                c.proximo = aux
            }
        }
    }
    
    /*
     *Insere primeiro elemento na lista
     */
    def addFirstCel(c:CelulaDouble[Int]): Unit = {
        topo = c
    }
    
    
    /*
     *Imprime Lista
     */
    def printList():Unit = {
        var aux = topo
        while(aux != null){
            print(aux.valor + " ")
            aux = aux.proximo
        }
        println() //Quebra linha
    }
    
    /*
     *Imprime Lista usando recursao
     */
    def printListRecursion(cel:CelulaDouble[Int]=topo):Unit = {
        if(cel!=null){
            print(cel.valor + " ")
            printListRecursion(cel.proximo)
        }
    }
    
    /*
     *Imprime da calda a cabeca da Lista
     */
    def printListReverse(cel:CelulaDouble[Int]=topo):Unit = {
        if(cel!=null){
            printListReverse(cel.proximo)
            print(cel.valor + " ")
        }
    }
    
    /*
     * Verifica se a lista esta vazia usando inteiros
     */
    def isEmpty(): Int = { 
      if(topo == null) 1 else 0}
    
    /*
     * Verifica se a lista esta vazia usando booleanos
     */
    def isEmptyB(): Boolean = { 
      if(topo == null) true else false}
    
    /*
     * Busca uma celula na lista
     */
    def search(value:Int): Int = { 
      var celaux = topo
      var contador = 0
      while(celaux!=null && celaux.valor != value){
        celaux = celaux.proximo
        contador+=1
      }
      if(celaux==null)
        contador = -1 //valor nao encontrado
      contador
    }

    
    /*
     * Remove um elemento da lista
     */
    def remove(index:Int):Unit = {
      if(!isEmptyB()){
        var ant:CelulaDouble[Int] = null
        var p = topo
        var contador = 0
        
        if(index == 0){
          p.proximo.anterior = p    
          topo = p.proximo
        }
        else{
          while(p!=null && contador != index-1){
             p = p.proximo
            contador+=1
          }
          if(p == null)
            println("valor nao encontrado")
          else {
                p.proximo = p.proximo.proximo
                p.proximo.anterior = p
          }
        }
      }
      else
        println("Lista Vazia")
    }
    
    
    /*
     * Remove um elemento da lista recursivamente
     */
    def removeRec(index:Int, c:CelulaDouble[Int]=topo, contador:Int=0):CelulaDouble[Int] = {
      var aux = c
      if(aux!=null){
        if(index==0){
          aux.proximo.anterior = topo.anterior
          topo = aux.proximo 
        }
        else{
            if(contador == index)
                aux = aux.proximo
            else{
              aux.proximo = removeRec(index,aux.proximo,contador+1)
              if(aux.proximo!=null)
                aux.proximo.anterior = aux
            } 
        }
      }
      aux //retorno
    }
    
    /*
     * Liberar a lista
     */
    def freeList():ListaDEOrdenada={
      topo = null
      this
    }
    
    /*
     * Compara duas listas
     */
    def equalsList(lst:ListaDEOrdenada):Boolean={
      var flag = true
      var auxl1 = topo
      var auxl2 = lst.topo
      while((auxl1 !=null || auxl2 !=null) && flag == true){
        if(auxl1.valor != auxl2.valor) //se algum valor das listas for diferente
          flag = false
        auxl1 = auxl1.proximo
        auxl2 = auxl2.proximo
      }
      if(auxl1 != null || auxl2 !=null)
        flag = false
      flag
    }
}
