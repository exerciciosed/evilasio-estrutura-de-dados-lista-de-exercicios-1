package ListaExercicios1.Questao1

object MainQuestao1 {
     def main(args: Array[String]) {
      
      var l:ListaEncadeada = null
       
      l = new ListaEncadeada()
      println("Lista Vazia Criada") 
       
      l.add(1)
      l.add(2)
      l.add(3)
      l.add(4)
      l.add(5)
      l.add(6)
      l.add(200)
      println("Valores 1 a 6 e 200 adicionados sucessivamente no topo da lista") 
      
      print("Lista impressa iterativamente: ")
      l.printList() 
      
      print("Lista impressa de tras pra frente: ")
      l.printListReverse()
      println()
      
      if(l.isEmpty() == 1)
        println("Lista vazia") 
      else 
        println("Lista nao vazia")
      
      println("O valor 6 esta na posicao "+ (l.search(6)+1))  

      l.remove(l.search(5))
      println("Removido o elemento de valor 5 iterativamente")
      
      l.removeRec(l.search(200))
      println("Removido o elemento de valor 200 recursivamente")

      print("Lista apos remocoes impressa recursivamente: ")
      l.printListRecursion()
      println()
      
      l.freeList()
      println("Lista liberada") 
   }
}