package ListaExercicios1.Questao1
import ListaExercicios1._

//---------- Lista ----------
class ListaEncadeada(var topo:Celula[Int]=null){
    
    /*
     *insere um elemento na primeira posicao da lista
     */
    def add(valor:Int){
        var c = new Celula[Int](valor, null)
        if(topo == null)
            addFirstCel(c)
        else{
            c.proximo = topo
            topo = c
        }
    }
    
    /*
     *Insere primeiro elemento na lista
     */
    def addFirstCel(c:Celula[Int]): Unit = {
        topo = c
    }
    
    
    /*
     *Imprime Lista
     */
    def printList():Unit = {
        var aux = topo
        while(aux != null){
            print(aux.valor + " ")
            aux = aux.proximo
        }
        println() //Quebra linha
    }
    
    /*
     *Imprime Lista usando recursao
     */
    def printListRecursion(cel:Celula[Int]=topo):Unit = {
        if(cel!=null){
            print(cel.valor + " ")
            printListRecursion(cel.proximo)
        }
    }
    
    /*
     *Imprime da calda a cabeca da Lista
     */
    def printListReverse(cel:Celula[Int]=topo):Unit = {
        if(cel!=null){
            printListReverse(cel.proximo)
            print(cel.valor + " ")
        }
    }
    
    /*
     * Verifica se a lista esta vazia usando inteiros
     */
    def isEmpty(): Int = { 
      if(topo == null) 1 else 0}
    
    /*
     * Verifica se a lista esta vazia usando booleanos
     */
    def isEmptyB(): Boolean = { 
      if(topo == null) true else false}
    
    /*
     * Busca uma celula na lista
     */
    def search(value:Int): Int = { 
      var celaux = topo
      var contador = 0
      while(celaux!=null && celaux.valor != value){
        celaux = celaux.proximo
        contador+=1
      }
      if(celaux==null)
        contador = -1 //valor nao encontrado
      contador
    }
    
    /*
     * Remove um elemento da lista
     */
    def remove(index:Int):Unit = {
      if(!isEmptyB()){
        var ant:Celula[Int] = null
        var p = topo
        var contador = 0
        
        if(index == 0)
              topo = p.proximo
        else{
          while(p!=null && contador != index-1){
             p = p.proximo
            contador+=1
          }
          if(p == null)
            println("valor nao encontrado")
          else {
                p.proximo = p.proximo.proximo
          }
        }
      }
      else
        println("Lista Vazia")
    }
    
    
    /*
     * Remove um elemento da lista recursivamente
     */
    def removeRec(index:Int, c:Celula[Int]=topo, contador:Int=0):Celula[Int] = {
      var aux = c
      if(aux!=null){
        if(index==0)
          topo = aux.proximo
        else{
            if(contador == index)
                aux = aux.proximo
            else
              aux.proximo = removeRec(index,aux.proximo,contador+1)
        }
      }
      aux //retorno
    }
    
    /*
     * Liberar a lista
     */
    def freeList():ListaEncadeada={
      topo = null
      this
    }
}
