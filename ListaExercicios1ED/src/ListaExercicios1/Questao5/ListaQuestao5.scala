package ListaExercicios1.Questao5
import ListaExercicios1._

//---------- Lista Duplamente Encadeada Circular----------//

class ListaDECircular(var topo:CelulaDouble[Int]=null){
    
    /*
     *insere um elemento na primeira posicao da lista
     */
    /*
     *insere um elemento na primeira posicao da lista
     */
    def add(valor:Int, pos:Int=0){
        var c = new CelulaDouble[Int](valor, null, null)
        if(topo == null)
            addFirstCel(c)
        else{
            var ant = topo
            var aux = topo
            var i = 0
            
            if(pos==0){ //Quando deve inserir na primeira posicao 
              c.proximo = aux
              c.anterior = aux.anterior
              aux.anterior = c
              topo = c
              
              //faz o ultimo apontar para o novo topo
              var ultimo = aux
              while(ultimo.proximo!=aux)
                ultimo = ultimo.proximo
              ultimo.proximo = topo
              
            }else{
              //vai ate a posicao solicitada ou ate o fim da lista
              while(i<pos && aux.proximo!=topo){
                ant = aux
                aux = aux.proximo
                i=i+1
              }
              
              if(aux.anterior == aux) //verifica se � o segundo elemento a ser adicionado
                aux.anterior = c
              
              c.proximo = aux.proximo
              c.anterior = aux
              aux.proximo = c
              
              if(c.proximo == topo) // verifica se foi adicionado no fim
                topo.anterior = c
            }        
         }
    }
    
    /*
     *Insere primeiro elemento na lista
     */
    def addFirstCel(c:CelulaDouble[Int]): Unit = {
        topo = c
        c.proximo = topo
        c.anterior = topo
    }
    
    /*
     *Imprime Lista
     */
    def printList():Unit = {
        var aux = topo
        while(aux.proximo!=topo){
            print(aux.valor + " ")
            aux = aux.proximo
        }
        println(aux.valor)
    }
    
    /*
     *Imprime Lista usando recursao
     */
    def printListRecursion(cel:CelulaDouble[Int]=topo):Unit = {
        if(cel.proximo!= topo){
            print(cel.valor + " ")
            printListRecursion(cel.proximo)
        }
        else
            println(cel.valor)
    }
    
    /*
     * Verifica se a lista esta vazia usando inteiros
     */
    def isEmpty(): Int = { 
      if(topo == null) 1 else 0}
    
    /*
     * Verifica se a lista esta vazia usando booleanos
     */
    def isEmptyB(): Boolean = { 
      if(topo == null) true else false}
    
    /*
     * Busca uma celula na lista
     */
    def search(value:Int): Int = { 
      var celaux = topo
      var contador = 0
      while(celaux.proximo!=topo && celaux.valor != value){
        celaux = celaux.proximo
        contador+=1
      }
      if(celaux.proximo==topo){
        if(celaux.valor != value)
          contador = -1 //valor nao encontrado
      }
      contador
    }
    
    /*
     * Remove um elemento da lista
     */
    def remove(index:Int):Unit = {
      if(!isEmptyB()){
        if(index == 0){
          if(topo.proximo==topo)
            topo = null
          else{
            topo.proximo.anterior = topo.anterior
            topo.anterior.proximo = topo.proximo
            topo = topo.proximo
          }
            
        }
        else{
          var contador = 0
          var p = topo.proximo
          contador+=1
          while(p!=topo && contador != index-1){
             p = p.proximo
             contador+=1
          }
          if(p == topo)
            println("valor nao encontrado")
          else {
                p.proximo.proximo.anterior = p
                p.proximo = p.proximo.proximo 
          }
        }
      }
      else
        println("Lista Vazia")
    }
    
    /*
     * Remove um elemento da lista recursivamente
     */
    def removeRec(index:Int, c:CelulaDouble[Int]=topo, contador:Int=0):CelulaDouble[Int] = {
      var aux = c
      if(aux.proximo!=topo){
        if(index==0){
          if(topo.proximo!=topo){
            topo.anterior.proximo = topo.proximo
            topo.proximo.anterior = topo.anterior
            topo = topo.proximo
          }
          else
            topo = null
        }
        else{
            if(contador == index)
                aux = aux.proximo
            else
              aux.proximo = removeRec(index,aux.proximo,contador+1)
              aux.proximo.anterior = aux
        }
      }
      else{
        if(contador != index)
          aux = topo
      }
      aux //retorno
    }
    
    /*
     * Liberar a lista
     */
    def freeList():ListaDECircular={
      topo = null
      this
    }
}
