package ListaExercicios1.Questao5

object MainQuestao5 {
     def main(args: Array[String]) {
      var l:ListaDECircular = null
       
      l = new ListaDECircular()
      println("Lista Vazia Criada") 
       
      l.add(1)
      l.add(2,1)
      l.add(3,2)
      l.add(4,3)
      l.add(5,4)
      l.add(6,5)
      l.add(200,6)

      println("Valores 1 a 6 e 200 adicionados sucessivamente na lista em ordem crescente") 
      
      print("Lista impressa iterativamente: ")
      l.printList()

      if(l.isEmpty() == 1)
        println("Lista vazia") 
      else 
        println("Lista nao vazia")
      
      println("O valor 6 est� na posicao "+ (l.search(6)+1))    

      l.remove(l.search(5))
      println("Removido o elemento de valor 5 iterativamente")
    
      l.remove(l.search(1))
      println("Removido o elemento de valor 1 recursivamente")
      
      l.remove(l.search(200))
      println("Removido o elemento de valor 200 recursivamente")

      print("Lista apos remocoes impressa recursivamente: ")
      l.printListRecursion()
      
      l.freeList()
      println("Lista liberada")
   }
}