package ListaExercicios1.Questao6

object MainQuestao6 {
     def main(args: Array[String]) {
      var l:ListaOrdenadaConta = null
       
      l = new ListaOrdenadaConta()
      //println("Lista Vazia Criada") 
      var conta1 = new ContaBancaria(1, 100) 
      var conta2 = new ContaPoupanca(2, 100) 
      var conta3 = new ContaFidelidade(3, 100) 
      
      l.add(conta1)
      l.add(conta2)
      l.add(conta3)
      println("Adicionada uma conta bancaria, uma conta poupanca e uma conta fidelidade a lista") 
      
      println("realizar credito na conta fidelidade")
      conta3.credito(20) 
      
      println("realizar credito na conta bancaria")
      conta1.debito(10) 
      
      println("Consultar o saldo da conta poupanca")
      println(conta2.saldo)
      
      println("Consultar o bonus da conta fidelidade")
      println(conta3.bonus)
      
      println("Tranfere 50 reais da conta bancaria para a conta fidelidade")
      conta1.tranferncia(conta1, conta3, 50)
      
      println("Render juros da conta Poupanca")
      conta2.renderJuros()
      
      println("Saldo da conta poupanca: "+conta2.saldo)
      
      println("Render bonus da conta fidelidade")
      conta3.renderBonus()
      
      println("Saldo da conta fidelidade: " + conta3.saldo)
      
      println("Remover conta bancaria")
      l.remove(conta1.numero)
      
      
      println("Imprime as contas presentes na lista")
      l.printList()
      
   }
}