package ListaExercicios1.Questao6

trait Conta {
  var numero:Long
  var saldo:Double
  def credito(valor:Double)
  def debito(valor:Double)
  def tranferncia(conta1:Conta, conta2:Conta, valor:Double)
}