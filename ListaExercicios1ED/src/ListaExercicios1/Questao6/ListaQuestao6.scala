package ListaExercicios1.Questao6

import ListaExercicios1.Celula

//---------- Lista Ordenada de Contas ----------//

class ListaOrdenadaConta(var topo:Celula[Conta]=null) {
  /*
     *insere um elemento na lista ordenada
     */
    def add(valor:Conta){
        var c = new Celula[Conta](valor, null)
        if(topo == null)
            addFirstCel(c)
        else{
            var ant:Celula[Conta] = null
            var aux = topo
            while(aux!=null && aux.valor.numero <= c.valor.numero){
              ant = aux
              aux = aux.proximo 
            }
            if(ant==null){ //Quando deve inserir na primeira posicao
              c.proximo = aux
              topo = c
            }else{
              ant.proximo = c
              if(aux!=null)//Caso nao seja o ultimo valor, faz o proximo de c apontar para aux
                c.proximo = aux
            }
        }
    }
    
    /*
     *Insere primeiro elemento na lista
     */
    def addFirstCel(c:Celula[Conta]): Unit = {
        topo = c
    }
    
    
    /*
     *Imprime Lista
     */
    def printList():Unit = {
        var aux = topo
        while(aux != null){
            var s = aux.valor.getClass.toString().split(" ")(1).split("ListaExercicios1.Questao6.")(1)
            println("Tipo da conta: " + s)
            println("Numero: "+aux.valor.numero)
            println("Saldo: "+aux.valor.saldo)
            aux = aux.proximo
        }
        println() //Quebra linha
    }
    
    /*
     *Imprime Lista usando recursao
     */
    def printListRecursion(cel:Celula[Conta]=topo):Unit = {
        if(cel!=null){
            var s = cel.valor.getClass.toString().split(" ")(1).split("ListaExercicios1.Questao6.")(1)
            println("Tipo da conta: " + s)
            println("Numero: "+cel.valor.numero)
            println("Saldo: "+cel.valor.saldo)
            printListRecursion(cel.proximo)
        }
    }
    
    /*
     *Imprime da calda a cabeca da Lista
     */
    def printListReverse(cel:Celula[Conta]=topo):Unit = {
        if(cel!=null){
            printListReverse(cel.proximo)
            print(cel.valor.numero + " ")
        }
    }
    
    /*
     * Verifica se a lista esta vazia usando inteiros
     */
    def isEmpty(): Int = { 
      if(topo == null) 1 else 0}
    
    /*
     * Verifica se a lista esta vazia usando booleanos
     */
    def isEmptyB(): Boolean = { 
      if(topo == null) true else false}
    
    /*
     * Busca uma celula na lista
     */
    def search(value:Long): Celula[Conta] = { 
      var celaux = topo
      while(celaux!=null && celaux.valor.numero != value){
        celaux = celaux.proximo
      }
      celaux
    }
    
    /*
     * Remove um elemento da lista
     */
    def remove(value:Long):Unit = {
      if(!isEmptyB()){
        var ant:Celula[Conta] = null
        var p = topo
        while(p!=null && p.valor.numero != value){
          ant = p
          p = p.proximo
        }
        if(p == null)
          println("valor nao encontrado")
        else {
            if(ant == null)
              topo = p.proximo     //Caso o valor esteja no primeiro elemento
            else
              ant.proximo = p.proximo
        }
      }
      else
        println("Lista Vazia")
    }
    
    
    /*
     * Remove um elemento da lista recursivamente
     */
    def removeRec(value:Long, c:Celula[Conta]=topo):Celula[Conta] = {
      var v = value
      var aux = c
      if(aux!=null){
        if(aux.valor.numero == value)
          if(aux == topo)
            topo = aux.proximo
          else
            aux = aux.proximo
        else
          aux.proximo = removeRec(v, aux.proximo)
      }
      aux //retorno
    }
    
    /*
     * Liberar a lista
     */
    def freeList():ListaOrdenadaConta={
      topo = null
      this
    }
    
}