package ListaExercicios1.Questao6

class ContaFidelidade(num:Long, s:Double) extends Conta  {
  override var numero:Long = num
  override var saldo:Double = s
  var bonus:Double = 0.0
  
  override def credito(valor:Double)={
    saldo = saldo + valor
    bonus = bonus + valor*0.01
  }
  
  override def debito(valor:Double)={
    if(saldo >= valor)
      saldo = saldo - valor
    else
      println("Saldo insuficiente")
  }
  
  override def tranferncia(conta1:Conta, conta2:Conta, valor:Double){
    if(conta1.saldo < valor)
      println("Saldo insuficiente")
    else{
      conta1.debito(valor)
      conta2.credito(valor)
    }
  }
  
  def renderBonus()={
    saldo += bonus
    bonus = 0.0
  }
  
}