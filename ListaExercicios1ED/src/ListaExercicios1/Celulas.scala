/*
 * Esse documento define as estruturas de celulas que serao usadas nas Questoes
 */
package ListaExercicios1

//------------- Celula das listas usadas na questoes 1,2,4 e 6 --------------//
class Celula[T](var valor:T, var proximo:Celula[T]){}

//----------- Celula das listas usadas nas questoes 3 e 5 -----------//
class CelulaDouble[T](var valor:T, var anterior:CelulaDouble[T], var proximo:CelulaDouble[T]){}


